import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import org.glassfish.grizzly.http.server.HttpServer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * JUnit test class to test the endpoints.HelloWorldEndPoint.
 * The @Before starts-up the web server.
 * The test requests the /helloworld endpoint.
 * This should pass through MyFilter, which doesn't happen.
 *
 * @author Nico Korthout
 * @version 1.0
 * @since 28-05-2015
 */
public class TestHelloWorldEndPoint {

    private static final URI BASE_URI = getBaseURI();

    private static URI getBaseURI() {
        return UriBuilder.fromUri("http://localhost/").port(9998).path("api").build();
    }

    private HttpServer server;
    private WebResource webservice;

    @Before
    public void startServer() throws IOException {
        server = TestApplicationSetup.startServer(BASE_URI);

        Client client = Client.create(new DefaultClientConfig());
        webservice = client.resource(BASE_URI);
    }

    @After
    public void stopServer() {
        server.stop();
    }

    @Test
    public void testHelloWorld() {
        // GET /helloworld
        ClientResponse response = webservice.path("helloworld")
                .accept(MediaType.APPLICATION_JSON)
                .type(MediaType.APPLICATION_JSON)
                .get(ClientResponse.class);
        System.out.println(response);

        // Check response code
        assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

        // Check if request went through MyFilter. (This sets the 'Via' header)
        assertTrue(response.getHeaders().containsKey("Via"));
        assertEquals("MyFilter", response.getHeaders().get("Via"));
    }

}
