import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;
import com.google.inject.servlet.ServletModule;
import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
import com.sun.jersey.api.core.DefaultResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.core.spi.component.ioc.IoCComponentProviderFactory;
import com.sun.jersey.guice.spi.container.GuiceComponentProviderFactory;
import org.glassfish.grizzly.http.server.HttpServer;

import java.io.IOException;
import java.net.URI;

/**
 * This class provides the Guice Injector for the test environment.
 * In addition it has a method to start the webserver for test purposes.
 *
 * @author Nico Korthout
 * @version 1.0
 * @since 28-05-2015
 */
public class TestApplicationSetup extends GuiceServletContextListener {

    public static HttpServer startServer(URI baseURI) throws IOException {
        System.out.println("\nStarting web service...");
        TestApplicationSetup testApplicationSetup = new TestApplicationSetup();
        ResourceConfig resourceConfig = new DefaultResourceConfig();
        IoCComponentProviderFactory ioCComponentProviderFactory =
                new GuiceComponentProviderFactory(resourceConfig, testApplicationSetup.getInjector());
        HttpServer httpServer =
                GrizzlyServerFactory.createHttpServer(baseURI, resourceConfig, ioCComponentProviderFactory);
        System.out.println(String.format("Web service has started. WADL available at %s/application.wadl", baseURI));
        return httpServer;
    }

    @Override
    protected Injector getInjector() {
        System.out.println("Configure for tests!");
        return Guice.createInjector(new ApplicationModule(), new ServletModule() {
            @Override
            protected void configureServlets() {
                super.configureServlets();
                // Here you can configure any tests specific bindings
            }
        });
    }

}