import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;
import com.google.inject.servlet.ServletModule;

/**
 * This class provides the Guice Injector for the production environment.
 * It is used by GuiceFilter, which is defined in the web.xml file.
 * This web.xml file in turn is only used in the production environment.
 *
 * @author Nico Korthout
 * @version 1.0
 * @since 28-05-2015
 */
public class ApplicationSetup extends GuiceServletContextListener {

    @Override
    protected Injector getInjector() {
        System.out.println("Configure for production!");
        return Guice.createInjector(new ApplicationModule(), new ServletModule() {
            @Override
            protected void configureServlets() {
                super.configureServlets();
                // Here you can configure any production specific bindings
            }
        });
    }

}
