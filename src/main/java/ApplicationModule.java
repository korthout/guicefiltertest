import com.google.inject.Singleton;
import com.google.inject.servlet.ServletModule;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.guice.spi.container.servlet.GuiceContainer;

/**
 * This Guice ServletModule configures and binds the Filter and the Jersey endpoints.
 * These configurations are application-wide and not specific to production or tests settings.
 *
 * @author Nico Korthout
 * @version 1.0
 * @since 28-05-2015
 */
public class ApplicationModule extends ServletModule {

    @Override
    protected void configureServlets() {
        super.configureServlets();

        // Filter all requests on path '/api' through MyFilter
        System.out.println("Binding MyFilter");
        bind(MyFilter.class).in(Singleton.class);
        filter("/api/*").through(MyFilter.class);

        // Configure Jersey's resource endpoints via Guice
        System.out.println("Binding Jersey resources");
        bind(GuiceContainer.class);
        ResourceConfig resourceConfig = new PackagesResourceConfig("endpoints");
        for (Class<?> resource : resourceConfig.getClasses()) {
            bind(resource);
        }
        serve("/api/*").with(GuiceContainer.class);
    }
}
