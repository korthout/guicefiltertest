package endpoints;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * Simple Jersey Endpoint
 *
 * @author Nico Korthout
 * @version 1.0
 * @since 28-05-2015
 */
@Path("/helloworld")
public class HelloWorldEndPoint {

    @GET
    public String get() {
        return "Hello World";
    }

}
