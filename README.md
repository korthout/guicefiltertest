# README #

This repository is hosting the example for a [StackOverflow question](http://stackoverflow.com/questions/30532667/requests-on-jersey-endpoints-are-not-filtered-through-filter-by-guices-servletm).

## Branches ##
* The master brach contains the example for the original question.
* The jtf-hk2 branch contains a workaround in which Guice was dropped. Instead, it uses Jersey Test Framework 2 with dependency injection by the HK2 library. The Filter does work here.